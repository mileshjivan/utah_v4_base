/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BDD.TestRunners;

import static KeywordDrivenTestFramework.Core.BaseClass.reportDirectory;
import KeywordDrivenTestFramework.Reporting.QTestScenarioReporter;
import java.io.File;
import static java.lang.System.err;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 *
 * @author Ferdinand
 */
@RunWith(Suite.class)
@SuiteClasses( { RunCucumberSmokeTests.class })
public class RunAllCucumberSuites_UpdateJiraResults {

    @AfterClass
    public static void report() {
          
        
        QTestScenarioReporter qtestReporter = new QTestScenarioReporter();

        qtestReporter.PushJSONReportToJira("target\\cucumber.json", 805679, 59623);

        try
        {
            File extentReport = new File(reportDirectory+ "\\ExtentReport.html");

            File extentReportDependancies = new File(reportDirectory + "\\extentreports");

            File htmlReportDirectory = new File("HTMLTestReport");

            if (!htmlReportDirectory.exists())
            {
                htmlReportDirectory.mkdirs();
            }

            FileUtils.copyFileToDirectory(extentReport, htmlReportDirectory);

            if (extentReportDependancies.exists())
            {
                FileUtils.copyDirectory(extentReportDependancies, htmlReportDirectory);
            }

        }
        catch (Exception ex)
        {
            err.println("[ERROR] Failed to copy extentReport file to HTML Test Report Directory - " + ex.getMessage());
        }
    
    }
    
    
    
    
    

}
