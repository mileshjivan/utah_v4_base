/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author Ferdinand
 */
public class ETLTestSuite 
{
    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public ETLTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;
       

    }

    @Test
    public void ETLTEST() throws FileNotFoundException
    {
        
        Narrator.logDebug("ETL - Test Pack");
        instance = new TestMarshall("TestPacks\\ETLTestPack.xlsx", Enums.BrowserType.Chrome);
        instance.requiresBrowser = false;
        instance.runKeywordDrivenTests();
    }
}
