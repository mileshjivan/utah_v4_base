/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.AttributeMapper_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteSignInPageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR1_AttributeMapper_MainScenario",
    createNewBrowserInstance = false
)

public class FR1_AttributeMapper_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;
   

  public FR1_AttributeMapper_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!attributeMapper())
        {
            return narrator.testFailed("Failed to map attributes: " + error);
        }
        return narrator.finalizeTest("Successfully mapped attributes");
    }

   
    public boolean attributeMapper() throws InterruptedException
    {   
         //Switch to Frame
        if (!SeleniumDriverInstance.swithToFrameByName(TrainingSiteSignInPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
            
        }
        
        //Attribue Mapper module
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.attributeMapper()))
        {
            error = "Failed to locate Attribue Mapper module";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.attributeMapper()))
        {
            error = "Failed to click on Attribue Mapper module";
            return false;
        }
        
        pause(8000);
         //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.addButtonXpath()))
        {
            error = "Failed to locate add button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.addButtonXpath()))
        {
            error = "Failed to click on add button ";
            return false;
        }
        
         //processflow
         pause(3000);
            if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add Attribute Record phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(AttributeMapper_PageObjects.processFlowStatus("Add Attribute Record"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add Attribute Record phase";
        return false;
        }

         //Module Template Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.moduleTemplateDropdown()))
        {
            error = "Failed to locate Module Template Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.moduleTemplateDropdown()))
        {
            error = "Failed to click on Module Template Dropdown";
            return false;
        }
    
       
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.singleSelect(testData.getData("Module Template value"))))
        {
            error = "Failed to locate Module Template Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.singleSelect(testData.getData("Module Template value"))))
        {
            error = "Failed to click Module Template Dropdown value";
            return false;
        }
        
         //Save button
         if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        } 
        
        pause(30000);
        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moves to pending update phase, status updated to pending update");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(AttributeMapper_PageObjects.processFlowStatusChild("Pending Update"),"class");
        if (!processStatusEdit.equalsIgnoreCase("step active")) {
        error = "Failed to move to Pending Update phase";
        return false;
        }
        
        String retrieveStatusText1 = SeleniumDriverInstance.retrieveTextByXpath(AttributeMapper_PageObjects.statusXpath());
        if (!retrieveStatusText1.equalsIgnoreCase("Pending Update")) {
            error = "Failed to retrieve Status text: Pending Update ";
            return false;
        }

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(AttributeMapper_PageObjects.attributeMapperRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());   
        
        //Update Types dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.updateTypesDropdown()))
        {
            error = "Failed to locate Update Types Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.updateTypesDropdown()))
        {
            error = "Failed to click on Update Types Dropdown";
            return false;
        }
        
        //Select M1 checkbox
        if(getData("Select M1 checkbox").equalsIgnoreCase("TRUE")){
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.checklistSelect("M1")))
        {
            error = "Failed to wait for M1 checkbox";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.checklistSelect("M1")))
        {
            error = "Failed to select M1 checkbox";
            return false;
        }
        }
        
        //Select M2 checkbox
        if(getData("Select M2 checkbox").equalsIgnoreCase("TRUE")){
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.checklistSelect("M2")))
        {
            error = "Failed to wait for M2 checkbox";
            return false;
        }
            
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.checklistSelect("M2")))
        {
            error = "Failed to select M2 checkbox";
            return false;
        }
        }
        
        //Select M3 checkbox
        if(getData("Select M3 checkbox").equalsIgnoreCase("TRUE")){
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.checklistSelect("M3")))
        {
            error = "Failed to wait for M3 checkbox";
            return false;
        }
            
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.checklistSelect("M3")))
        {
            error = "Failed to select M3 checkbox";
            return false;
        }
        }
        
        
        //M1
        if(!getData("M1 values").equals("FALSE"))
        {
        String[] M1_Values = getData("M1 values").split("\\|");
        String[] M1_rowNumbers = M1_Values[0].split(",");
        String[] M1_row_data = M1_Values[1].split(",");
        for (int row = 0; row< M1_rowNumbers.length; row++){
        if(!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.M1Elements(M1_rowNumbers[row])))
        {
            error = "Failed to enter text in M1 field: " + M1_rowNumbers[row];
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(AttributeMapper_PageObjects.M1Elements(M1_rowNumbers[row])+"//input",M1_row_data[row]))
        {
            error = "Failed to enter text in M1 field: " + M1_rowNumbers[row];
            return false;
        }
        }
        }
        
        //M2
        if(!getData("M2 values").equals("FALSE"))
        {
        String[] M2_Values = getData("M2 values").split("\\|");
        String[] M2_rowNumbers = M2_Values[0].split(",");
        String[] M2_row_data = M2_Values[1].split(",");
        for (int row = 0; row< M2_rowNumbers.length; row++){
        if(!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.M1Elements(M2_rowNumbers[row])))
        {
            error = "Failed to enter text in M1 field: " + M2_rowNumbers[row];
            return false;
        }
            
        if (!SeleniumDriverInstance.enterTextByXpath(AttributeMapper_PageObjects.M2Elements(M2_rowNumbers[row])+"//input",M2_row_data[row]))
        {
            error = "Failed to enter text in M2 field: " + M2_rowNumbers[row];
            return false;
        }
        }
        }
        
         //M3
        if(!getData("M3 values").equals("FALSE"))
        {
        String[] M3_Values = getData("M3 values").split("\\|");
        String[] M3_rowNumbers = M3_Values[0].split(",");
        String[] M3_row_data = M3_Values[1].split(",");
        for (int row = 0; row< M3_rowNumbers.length; row++){
        if(!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.M1Elements(M3_rowNumbers[row])))
        {
            error = "Failed to enter text in M3 field: " + M3_rowNumbers[row];
            return false;
        }
            
        if (!SeleniumDriverInstance.enterTextByXpath(AttributeMapper_PageObjects.M3Elements(M3_rowNumbers[row])+"//input",M3_row_data[row]))
        {
            error = "Failed to enter text in M3 field: " + M3_rowNumbers[row];
            return false;
        }
        }
        }
        
        //Attribute mapping confirmation checkbox
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.confirmationCheckbox())) {
            error = "Failed to wait for Attribute mapping confirmation checkbox";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.confirmationCheckbox())) {
            error = "Failed to click on Attribute mapping confirmation checkbox";
            return false;
        }
        
        //Implement Attribute changes button
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.implementChangesButton()))
        {
            error = "Failed to locate Implement Attribute changes button";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.implementChangesButton()))
        {
            error = "Failed to click Implement Attribute changes button";
            return false;
        } 
         
        pause(30000);
        narrator.stepPassedWithScreenShot("Successfully changed the status and processflow to Synchronised");
        String processStatus4 = SeleniumDriverInstance.retrieveAttributeByXpath(AttributeMapper_PageObjects.processFlowStatusChild("Synchronised"),"class");
        if (!processStatus4.equalsIgnoreCase("step active")) {
        error = "Failed to move to Synchronised phase";
        return false;
        }
        
        String retrieveStatusText = SeleniumDriverInstance.retrieveTextByXpath(AttributeMapper_PageObjects.statusXpath());
        if (!retrieveStatusText .equalsIgnoreCase("Synchronised")) {
            error = "Failed to retrieve Status text: Synchronised ";
            return false;
        }
        return true;
        
        

        
    }
}


