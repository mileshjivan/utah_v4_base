/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.AttributeMapper_PageObjects;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR2_VerifyModuleTempate_MainScenario",
    createNewBrowserInstance = false
)

public class FR2_VerifyModuleTempate_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;
   

  public FR2_VerifyModuleTempate_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!attributeMapper())
        {
            return narrator.testFailed("Failed to verify mapped attributes: " + error);
        }
        return narrator.finalizeTest("Successfully verified mapped attributes");
    }

   
    public boolean attributeMapper() throws InterruptedException
    {    
         
       //Side menu
        if(!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.isoSideMenu())){
            error = "Failed to wait for Side Menu.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.isoSideMenu())){
            error = "Failed to click Side Menu.";
            return false;
        }
        
        
        //System
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.system()))
        {
            error = "Failed to locate System option on side menu";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.system()))
        {
            error = "Failed to click on System option on side menu";
            return false;
        }
        
        //Close Side menu
        if(!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.isoSideMenu())){
            error = "Failed to wait to close Side Menu.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.isoSideMenu())){
            error = "Failed to click Side Menu icon to close it.";
            return false;
        }
        
       
        //Switch to frame
         if (!SeleniumDriverInstance.swithToFrameByName(AttributeMapper_PageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
            
        }

        
        //Module templates checkbox
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.moduleTemplatesCheckbox()))
        {
            error = "Failed to locate Module templates checkbox";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.moduleTemplatesCheckbox()))
        {
            error = "Failed to click on Module templates checkbox";
            return false;
        }
        
        pause(5000);
        
        //Right click on template
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.rightClickOnTemplate(testData.getData("Module template"))))
        {
            error = "Failed to locate module template";
            return false;
        }
        
        if (!SeleniumDriverInstance.rightClickElementbyXpathUsingActions(AttributeMapper_PageObjects.rightClickOnTemplate(testData.getData("Module template"))))
        {
            error = "Failed to right click on module template";
            return false;
        }
        
        //Edit module template
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.editTemplate()))
        {
            error = "Failed to click on Edit module template";
            return false;
        }
        
        pause(5000);
        String[] Field_Name = getData("Field Name").split(",");
        for (int i = 0; i < Field_Name.length; i++){
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.fieldName(Field_Name[i]))){
            error = "Failed to wait for field: " + Field_Name[i];
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.fieldName(Field_Name[i]))){
             error = "Failed to click the field: " + Field_Name[i];
             return false;
        }
        
        //Attributes panel
        if (!SeleniumDriverInstance.waitForElementByXpath(AttributeMapper_PageObjects.attributesPanel()))
        {
            error = "Failed to wait for attributes panel";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(AttributeMapper_PageObjects.attributesPanel()))
        {
            error = "Failed to click on attributes panel";
            return false;
        }
        
        pause(3000);
        //Scroll down
        JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
        js.executeScript("window.scrollBy(0,1000)");
        
        narrator.stepPassedWithScreenShot("Showing the attribute numbers");
        }
                

        
        
        return true;
        
        

        
    }

}
