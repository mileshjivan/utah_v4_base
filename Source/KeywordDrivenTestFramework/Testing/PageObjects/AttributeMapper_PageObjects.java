/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

//import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.*;
//import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.AttributeMapper_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class AttributeMapper_PageObjects extends BaseClass
{
    
    public static String attributeMapper()
    {
        return "//div[@original-title='Attribute Mapper']";
    }
    
     public static String addButtonXpath()
    {
        return "//div[@id='btnActAddNew']";
    }
     
     public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_82095348-9CC9-4818-9DFA-6E80855F8FA4']";
    }

     public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }

     public static String processFlowStatusChild(String phase)
    {
    return "(//div[text()='"+phase+"']/parent::div)[2]";
    } 
             
    public static String moduleTemplateDropdown()
    {
        return "//div[@id='control_35EDFC9A-2CE1-45C4-9F0D-AA39A4D56E51']//li";
    }  
      
    public static String singleSelect(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='"+value+"']";
    } 
    
    public static String saveButton()
    {
        return "//div[@id='btnSave_form_82095348-9CC9-4818-9DFA-6E80855F8FA4']";
    } 
   
    public static String attributeMapperRecordNumber_xpath() {
        return "(//div[@id='form_82095348-9CC9-4818-9DFA-6E80855F8FA4']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String updateTypesDropdown()
    {
        return "//div[@id='control_CBF82070-E307-42EE-9852-EAAE4801E5CC']//li";
    } 
    
    public static String checklistSelect(String text)
    {
         return "(//a[text()='" + text + "']/i[1])";
    }
    
    public static String M1Textbox(String index)
    {
       return "(//div[@id='control_FE289A69-1D5C-4A35-B06E-7DEF072AC356']//input)["+index+"]";
    }
    
    public static String M1Elements(String rowNumber)
    {
       return "(.//a[text()='M1']/../../../../../../following-sibling::div//table//td[8])["+rowNumber+"]";
    }
    
    public static String M2Elements(String rowNumber)
    {
       return "(.//a[text()='M2']/../../../../../../following-sibling::div//table//td[9])["+rowNumber+"]";
    }
    
    public static String M3Elements(String rowNumber)
    {
       return "(.//a[text()='M3']/../../../../../../following-sibling::div//table//td[10])["+rowNumber+"]";
    }
    
    public static String entityTypeM1Textbox()
    {
       return "(//div[@id='control_FE289A69-1D5C-4A35-B06E-7DEF072AC356']//input)[1]";
    }
    
    public static String entityTypeM2Textbox()
    {
       return "(//div[@id='control_4839B02C-620C-4B05-BD86-B6585DF55CC9']//input)[1]";
    }
    
    public static String entityTypeM3Textbox()
    {
       return "(//div[@id='control_747BBD9D-7B34-4E17-AD08-DB20958DD2B8']//input)[1]";
    }
    
    public static String confirmationCheckbox(){
        return "//div[@id='control_6644D1D0-C1A1-4408-A751-28B4BB41EEA5']//div[@class='c-chk']//div";
    }
    
    public static String implementChangesButton()
    {
        return "//div[@id='control_312BC551-0E93-437F-BE64-64F5DF79B507']";
    }         
    
    public static String statusXpath()
    {
        return "//div[@id='control_F64795CF-6120-4624-A163-F1333B62BB64']//li";
    }         
    
    //FR2
    public static String isoSideMenu(){
        return "//i[@class='large sidebar link icon menu']";
    }
    
    public static String system()
    {
        return "//div[@title='System']";
    }
    
    public static String moduleTemplatesCheckbox()
    {
        return "(//div[contains(@class,'iradio icheck-item icheck')])[4]";
    }
    
    public static String iframeName()
    {
        return "ifrMain";
    }
    
    public static String rightClickOnTemplate(String text)
    {
        return "//a[text()='"+text+"']";
    }
    
    public static String editTemplate()
    {
        return "//a[text()='Edit module template']";
    }
    
    public static String fieldName(String text)
    {
        return "(//span[text()='"+text+"'])[1]";
    }
    
    public static String attributesPanel()
    {
        return "//span[@title='Attributes']";
    }
    
    //FR3
    public static String viewFilter()
    {
        return "//div[@id='btnActFilter']";
    }
    
    public static String templateNamedropdown()
    {
        return "(//span[@class='select3-arrow']//b)[1]";
    }
    
    public static String moduleTemplateName(String text)
    {
        return "(//a[text()='"+text+"']//i)[1]";
    }
    
    public static String searchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }  
    
    public static String recordToBeOpened(String text)
    {
        return "(//div[text()='"+text+"'])[1]";
    }
    
//    public static String recordToBeOpened(String recordNumber)
//    {
//        return "//span[text()='"+recordNumber+"'][@type='recordnumber']/../parent::tr";
//    }  
    
    public static String unlockCheckbox(){
        return "//div[@id='control_29282DCC-5BAD-4291-8BD7-030798D7FB01']//div[@class='c-chk']//div";
    }
    
    public static String unlockMappings(){
        return "//div[@id='control_B197CA78-EEB5-40A9-8796-88C08B7E0D27']";
    }
    
    public static String updateTypes()
    {
        return "(//div[@id='control_CBF82070-E307-42EE-9852-EAAE4801E5CC']//span//b)[1]";
    } 
    
    
}
